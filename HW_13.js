/**
 * Created by Алина on 15.01.2022.
 */

const themeLink1 = "./css/style.css";
const themeLink2 = "./css/style_2.css";
let link = document.getElementById("style");
if (!localStorage.getItem("theme")) {
    localStorage.setItem("theme", themeLink1)
}
link.setAttribute("href", localStorage.getItem("theme"));
document.querySelector(".change-color-theme").addEventListener(
    "click", function () {
        if (link.getAttribute("href") == themeLink1) {
            link.setAttribute("href", themeLink2);
            localStorage.setItem("theme", `${link.getAttribute("href")}`);
        } else if (localStorage.getItem("theme") == themeLink2) {
            link.setAttribute("href", themeLink1);
            localStorage.clear()
        }
    })


